#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include <map>
#include <string>
#include <iostream>
#include "Utils.h"
#include "RedisHandler.h"
#include "Notifier.h"

namespace Application {
    class Controller
    {
        public:
            Controller();
            ~Controller();
            void execute(char* args[]);
            static void showHelp();
        private:
            const std::map<std::string,int> command = {{"run",1},{"down",2},{"help",3}};
            int getArgMappingKey(std::string arg);
            void startNotifier(char* args[]);
    };
}

#endif