#include "NotificationHandler.h"

using namespace Application;

NotificationHandler::NotificationHandler() {
    notify_init("NotificationHandler");
}

NotificationHandler::~NotificationHandler() {
    notify_uninit();
}

void NotificationHandler::sendMessage(std::string header, std::string body) {
    NotifyNotification* notification = notify_notification_new(header.c_str(), body.c_str(), "");
    notify_notification_show(notification,nullptr);
    g_object_unref(G_OBJECT(notification));
}

void NotificationHandler::sendMessage(std::string* header, std::string* body) {
    NotifyNotification* notification = notify_notification_new(header->c_str(), body->c_str(), "");
    notify_notification_show(notification,nullptr);
    g_object_unref(G_OBJECT(notification));
}