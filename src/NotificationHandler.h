#ifndef NOTIFICATION_HANDLER_H_
#define NOTIFICATION_HANDLER_H_

#include <string>
#include <libnotify/notify.h>

namespace Application{
    class NotificationHandler
    {
        public:
            NotificationHandler();
            ~NotificationHandler();
            void sendMessage(std::string header, std::string body);
            void sendMessage(std::string* header, std::string* body);
    };
}

#endif