#ifndef REDIS_HANDLER_H_
#define REDIS_HANDLER_H_

#include <string>
#include <sw/redis++/redis++.h>

namespace Application {
    class RedisHandler {
        public:
            RedisHandler(std::string connection);
            RedisHandler(std::string* connection);
            ~RedisHandler();
            std::string* consume(std::string qName);
            std::string* consume(std::string* qName);
            std::string* consume(char* qName);
        private:
            sw::redis::Redis* client;
    };
}

#endif