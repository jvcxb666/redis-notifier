#include "Utils.h"

using namespace Application;

static const std::string CONFIG_FILE = "/usr/lib/notifier.conf";
static const std::string CACHE_FILE = "~/notifier.cache";

std::string Utils::getIpByContainerName(std::string name) {
    std::string command = "sudo docker inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ";
    command.append(name);
    return getCmdResult(command.c_str());
}

std::string Utils::getIpByContainerName(std::string* name) {
    std::string command = "sudo docker inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ";
    command.append(*name);
    return getCmdResult(command.c_str());
}

std::string Utils::getPortByContainerName(std::string name) {
    std::string command = "sudo docker inspect --format '{{ (index (index .NetworkSettings.Ports \"6379/tcp\") 0).HostPort }}' ";
    command.append(name);
    return getCmdResult(command.c_str());
}

std::string Utils::getPortByContainerName(std::string* name) {
    std::string command = "sudo docker inspect --format '{{ (index (index .NetworkSettings.Ports \"6379/tcp\") 0).HostPort }}' ";
    command.append(*name);
    return getCmdResult(command.c_str());
}

std::string Utils::getRedisContainerConnectionString(std::string container) {
    std::string connection = "tcp://";
    connection.append(Utils::getIpByContainerName(container));
    connection.append(":");
    connection.append(Utils::getPortByContainerName(container));
    return connection;
}

std::string Utils::getRedisContainerConnectionString(std::string* container) {
    std::string connection = "tcp://";
    connection.append(Utils::getIpByContainerName(container));
    connection.append(":");
    connection.append(Utils::getPortByContainerName(container));
    return connection;
}

std::string Utils::getRedisServerConnectionString() {
    std::string connection = "tcp://";
    connection.append(Utils::getConfigVariable("server_redis_username"));
    connection.append(":");
    connection.append(Utils::getConfigVariable("server_redis_password"));
    connection.append("@");
    connection.append(Utils::getConfigVariable("server_redis_ip"));
    connection.append(":");
    connection.append(Utils::getConfigVariable("server_redis_port"));
    return connection;
}

std::string Utils::getConfigVariable(std::string key) {
    std::ifstream stream;

    stream.open(CONFIG_FILE);

    if(stream.is_open()) {
        std::string* line = new std::string;

        while(std::getline(stream,*line)){
            if(line->length() > 1 && line->substr(0,1) != "#" && line->find("=") != std::string::npos){
                if(line->substr(0,line->find("=")).find(key) != std::string::npos){
                    return line->substr(line->find("=") + 1 ,line->length());
                }
            }
        }

        stream.close();
        delete line;
    }

    std::cout << "FATAL ERROR: config key " << key << " does not exist \n";
    exit(0);
}

void Utils::writePid(std::string pid) {
    Utils::createCacheFile();
    std::string cmd = "sudo echo -n \"";
    cmd.append(pid);
    cmd.append(" \" >> ");
    cmd.append(CACHE_FILE);
    system(cmd.c_str());
}

void Utils::writePid(std::string* pid) {
    Utils::createCacheFile();
    std::string cmd = "sudo echo -n \"";
    cmd.append(*pid);
    cmd.append(" \" >> ");
    cmd.append(CACHE_FILE);
    system(cmd.c_str());
}

void Utils::killDaemonsFromCache() {
    std::string cmd = "sudo kill "; 
    std::string cat = "sudo cat ";
    cat.append(CACHE_FILE);
    cmd.append(Utils::getCmdResult(cat.c_str()));
    system(cmd.c_str());
    Utils::createCacheFile(true);
}

void Utils::createCacheFile(bool erase) {
    std::string touch = "sudo touch ";
    std::string clear = "sudo truncate -s 0 ";
    touch.append(CACHE_FILE);
    clear.append(CACHE_FILE);
    system(touch.c_str());
    if(erase)system(clear.c_str());
}

std::string Utils::getCmdResult(const char* command)
{
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(command, "r"), pclose);
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        if(buffer.data() != "/n")
        result += buffer.data();
    }
    return (result.length() > 0) ? result.erase(result.length()-1) : "";
}