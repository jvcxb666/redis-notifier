#ifndef UTILS_H_
#define UTILS_H_

#include <iostream>
#include <string>
#include <memory>
#include <array>
#include <fstream>

namespace Application {
    class Utils {
        public:
            static std::string getIpByContainerName(std::string name);
            static std::string getIpByContainerName(std::string* name);
            static std::string getPortByContainerName(std::string name);
            static std::string getPortByContainerName(std::string* name);
            static std::string getRedisContainerConnectionString(std::string container);
            static std::string getRedisContainerConnectionString(std::string* container);
            static std::string getRedisServerConnectionString();
            static std::string getConfigVariable(std::string key);
            static void writePid(std::string pid);
            static void writePid(std::string* pid);
            static void killDaemonsFromCache();
        private: 
            static std::string getCmdResult(const char* command);
            static void createCacheFile(bool erase = false);
    };
}

#endif