#include "Controller.h"

using namespace Application;

const std::map<std::string,int> command;

Controller::Controller() {

}

Controller::~Controller() {
    
}

void Controller::execute(char* args[]) {
    switch (getArgMappingKey(args[1]))
    {
    case 1:
        startNotifier(args);
        break;
    case 2:
        Utils::killDaemonsFromCache();
        break;
    case 3:
        Controller::showHelp();
        break;
    default:
        std::cout << "Command '" << args[1] << "' not supported" << "\n";
        showHelp();
        break;
    }
}

int Controller::getArgMappingKey(std::string arg) {
    try{
        return command.at(arg);
    }catch(std::exception& e){
        return -1;
    }
}

void Controller::showHelp() {
    std::cout << "Notification service:" << "\n";
    std::cout << "  up - start service" << "\n";
    std::cout << "      -c start with connection from config" << "\n";
    std::cout << "      -cd start with container from config (for local use i guess)" << "\n";
    std::cout << "          --d start as daemon" << "\n";
    std::cout << "  down - terminate all daemon instances" << "\n";
}

void Controller::startNotifier(char* args[]) {
    std::string connection = "";
    int daemon = (args[3] != nullptr && std::string(args[3]) == "--d") ? 1 : 0;
    if(std::string(args[2]) == "-c") {
        connection = Utils::getRedisServerConnectionString();
    } else if (std::string(args[2]) == "-cd") {
        connection = Utils::getRedisContainerConnectionString(Utils::getConfigVariable("container_redis_name"));
    } else {
        std::cout << "Argument '" << args[2] << "' not suported" << "\n";
        exit(0);
    }

    RedisHandler* handler = new RedisHandler(connection);
    Notifier* notifier = new Notifier(handler);
    notifier->run(daemon);

    delete notifier;
    delete handler;
}