#include "RedisHandler.h"

using namespace Application;

sw::redis::Redis* client;

RedisHandler::RedisHandler(std::string connection) {
    client = new sw::redis::Redis(connection);
}

RedisHandler::RedisHandler(std::string* connection) {
    client = new sw::redis::Redis(*connection);
}

RedisHandler::~RedisHandler() {
    delete client;
}

std::string* RedisHandler::consume(char* qName) {
    sw::redis::OptionalString res = client->lpop(qName);
    if(res) {
        return new std::string(res.value());
    } else {
        return new std::string("");
    }
}

std::string* RedisHandler::consume(std::string qName) {
    sw::redis::OptionalString res = client->lpop(qName.c_str());
    if(res) {
        return new std::string(res.value());
    } else {
        return new std::string("");
    }
}

std::string* RedisHandler::consume(std::string* qName) {
   sw::redis::OptionalString res = client->lpop(qName->c_str());
    if(res) {
        return new std::string(res.value());
    } else {
        return new std::string("");
    }
}