#ifndef NOTIFIER_H_
#define NOTIFIER_H_

#include <iostream>
#include "RedisHandler.h"
#include "NotificationHandler.h"
#include "Utils.h"

namespace Application {
    class Notifier
    {
        public:
            Notifier(RedisHandler* redisConnection);
            ~Notifier();
            void run(int daemon);
        private:
            RedisHandler* redis;
            void consume();
            std::string queue;
            std::string applicationName;
            int sleepTime;
    };
}

#endif