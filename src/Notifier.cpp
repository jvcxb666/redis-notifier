#include "Notifier.h"

using namespace Application;

RedisHandler* redis;
std::string queue;
std::string applicationName;
int sleepTime;

Notifier::Notifier(RedisHandler* redisConnection) {
    redis = redisConnection;
    queue = Utils::getConfigVariable("queue");
    applicationName = Utils::getConfigVariable("application_name");
    int sleep = std::stoi(Utils::getConfigVariable("sleep_time"));
    sleepTime = sleep >= 1 ? sleep : 1;
}

Notifier::~Notifier() {
    delete redis;
}

void Notifier::run(int daemon) {
    if(daemon > 0) {
        pid_t pid = fork(); 

        if(pid > 0) {
            exit(0);
        }else{
            Utils::writePid(std::to_string(getpid()));
            consume();
        }
    }else{
        consume();
    }
}

void Notifier::consume() {
    Application::NotificationHandler notifications;
    while(true) {
        std::string* result = redis->consume(queue);
        if(result != nullptr && !result->empty()) {
            notifications.sendMessage(applicationName,*result);
        }
        delete result;
        sleep(sleepTime);
    }
}