<h1>Notification service</h1>
<p>Daemon based on redis queue</p>
<h2>Changelog:</h1>
<h3>version 1.1</h3>
<ul>
    <li>added configuration option for sleep time in consumer</li>
    <li>fixed almost all memory leaks</li>
</ul>
<h2>Requirements:</h2>
<ul>
<li><b>hiredis</b> - https://github.com/redis/hiredis</li>
<li><b>Redis++</b> - https://github.com/sewenew/redis-plus-plus</li>
<li><b>glib-2.0</b></li>
<li><b>gdk-pixbuf-2.0</b></li>
<li><b>gtk-3.0</b></li>
<li><b>libnotify-dev</b></li>
</ul>
<h2>Installation:</h2>
<ul>
<li> clone this project</li>
<li> clone & install requirements</li>
<li> cd into project folder \build</li>
<li> run <b><i>cmake install .<i></b></li>
<li> run <b><i>make</i></b></li>
<li> copy <b><i>config_reference</i></b> to <b><i>/usr/lib/notifier.conf</i></b> and edit for your system</li>
</ul>
<h2>Usage</h2>
<ul>
    <li> <b><i>Notifier run -c</i></b> connect to redis server and run foreground</li>
    <li> <b><i>Notifier run -c --d</i></b> connect to redis server and run as daemon</li>
    <li> <b><i>Notifier run -cd</i></b> connect to redis container and run foreground</li>
    <li> <b><i>Notifier run -cd --d</i></b> connect to redis container and run as daemon</li>
    <li> <b><i>Notifier down</i></b> terminate all existing daemons</li>
</ul>

