#include <iostream>
#include "src/Controller.h"

using namespace Application;

int main(int argc, char *argv[]) {

    if(argc > 1) {
        try{
            Controller controller;
            controller.execute(argv);
        } catch (std::exception& e) {
            std::cout << "\033[0;31m ERROR: \033[0m" << e.what() << "\n";
            return 0;
        }
    } else {
        std::cout << "No arguments provided" << "\n";
        Application::Controller::showHelp();
    }

    return 0;
}